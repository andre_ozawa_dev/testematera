package br.com.andreozawa.testematera.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.andreozawa.testematera.model.Aluno;
import br.com.andreozawa.testematera.model.Notas;

@Repository
public class AlunosRepository {

	// recuperar alunos: GET​: /alunos
	public List<Aluno> getAll() {
		List<Aluno> alunos = new ArrayList<Aluno>();

		for (int i = 0; i < 50; i++) {
			// hack alunos
			Aluno aluno = new Aluno("", "Aluno " + (i + 1), "Rua aluno " + (i + 1), "", "email@gmail.com");
			alunos.add(aluno);
		}

		return alunos;
	}

	// recuperar notas do aluno: GET​: /alunos/#{cpf}/notas
	public Notas getNotas(Aluno aluno) {
		Notas notas = new Notas();
		notas.setAluno(aluno);
		notas.getDisciplinas();
		
		return notas;
	}
}
