package br.com.andreozawa.testematera.controllers;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.andreozawa.testematera.model.Aluno;
import br.com.andreozawa.testematera.model.Notas;
import br.com.andreozawa.testematera.services.AlunosService;

@RestController
public class MalaDiretaController {

	@Autowired
	private AlunosService alunosServce;

	private static final float AVARAGE_REQUIRED = 7;

	@PostMapping("/maladireta")
	public ResponseEntity<?> malaDireta(@RequestBody Aluno alunoo) {
		int emailsSent = 0;

		try {
			List<Aluno> alunos = this.alunosServce.getAll();

			Properties props = this.getProperties();
			
			for (Aluno aluno : alunos) {
				boolean isSendEmail = false;

				Notas notas = this.alunosServce.getNotas(aluno);

				for (int i = 0; i < notas.getDisciplinas().size() && !isSendEmail; i++) {
					if (notas.getDisciplinas().get(i).getNota() < AVARAGE_REQUIRED) {
						isSendEmail = true;
					}
				}

				if (isSendEmail) {
					this.sendEmail(props, aluno);

					emailsSent++;
				}
			}

		} catch (Exception e) {
			return new ResponseEntity<String>(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Integer>(emailsSent, HttpStatus.OK);
	}

	private void sendEmail(Properties props, Aluno aluno) throws MessagingException {
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(props.getProperty("mail.from"), props.getProperty("mail.password"));
			}
		});
		
		Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(props.getProperty("mail.from")));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(aluno.getEmail()));
        message.setSubject("Estude mais");
        message.setText("Você possui nota abaixo da média");

        Transport.send(message);
	}

	private Properties getProperties() {
		Properties props = new Properties();

		// TODO configurar com dados do servidor
		props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.socketFactory.port", "587");
	    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.port", "587");
	    props.put("mail.smtp.starttls.enable", "true"); 
	    props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
	    props.put("mail.smtp.socketFactory.fallback", "true");
	    props.put("mail.smtp.ssl.socketFactory", "true");
	    props.put("mail.smtp.EnableSSL.enable","true");
	    
	    // TODO configurar conta
	    props.put("mail.from","andre.ozawa.dev@gmail.com");
	    props.put("mail.password","");
	    
		return props;
	}

}
