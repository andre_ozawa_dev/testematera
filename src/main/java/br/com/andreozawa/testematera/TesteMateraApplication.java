package br.com.andreozawa.testematera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteMateraApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteMateraApplication.class, args);
	}
}
