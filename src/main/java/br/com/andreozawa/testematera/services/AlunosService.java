package br.com.andreozawa.testematera.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.andreozawa.testematera.model.Aluno;
import br.com.andreozawa.testematera.model.Notas;
import br.com.andreozawa.testematera.repository.AlunosRepository;

@Repository
public class AlunosService {

	@Autowired
	private AlunosRepository alunosRepository;
	
	public List<Aluno> getAll() {
		return this.alunosRepository.getAll();
	}
	
	public Notas getNotas(Aluno aluno) {
		return this.alunosRepository.getNotas(aluno);
	}
}
