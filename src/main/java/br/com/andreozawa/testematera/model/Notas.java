package br.com.andreozawa.testematera.model;

import java.util.ArrayList;
import java.util.List;

public class Notas {

	private Aluno aluno;
	private List<Disciplina> disciplinas;

	public Notas() {
		this.disciplinas = new ArrayList<Disciplina>();

		for (int i = 0; i < 8; i++) {
			Disciplina disciplina = new Disciplina("Diciplina " + (i + 1), (float) Math.random());

			this.disciplinas.add(disciplina);
		}
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aluno == null) ? 0 : aluno.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Notas other = (Notas) obj;
		if (aluno == null) {
			if (other.aluno != null)
				return false;
		} else if (!aluno.equals(other.aluno))
			return false;
		return true;
	}

}
